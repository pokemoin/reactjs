const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

const jsSourcePath = path.join(__dirname, './source/js/index.js');
const buildPath = path.join(__dirname, './build/');
const imgPath = path.join(__dirname, './source/assets/img/');
const scssPath = path.join(__dirname, './source/scss/style.scss');
const sourcePath = path.join(__dirname, './source/');
const mainHtmlTemplate = path.join(__dirname, './source/templates/index.ejs');

module.exports = {
  entry: {
    'app': [
      'babel-polyfill',
      'react-hot-loader/patch',
      jsSourcePath,
      scssPath
    ]
  },
  output: {
    path: path.resolve(__dirname, buildPath),
    filename: 'bundle.js',
    publicPath: "/"
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
        ],
      },
      {
        test: /\.(png|gif|jpg|svg)$/,
        loader: 'url-loader?limit=20480&name=assets/[name]-[hash].[ext]',
      },
      {
        test: /\.ico$/,
        loader: 'file-loader?name=[name].[ext]'
      },
      // {
      //   test: /\.css$/,
      //   loader: "style-loader!css-loader!autoprefixer-loader",
      // },
      {
        test: /\.json$/,
        use: 'json-loader'
      },
      {
        test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        loader: 'file-loader?name=fonts/[name].[ext]'
      },
      {
        test: /\.(sass|scss)$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader", "sass-loader"]
        })
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    stats: 'minimal',
    open: true,
    hot: true,
    historyApiFallback: true,
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'style.bundle.css',
      allChunks: true,
    }),
    new HtmlWebpackPlugin({
      favicon: './source/assets/img/favicon.ico',
      title: 'Nikolai Verbitskii | MEDIA ASYNC',
      template: path.resolve(__dirname, mainHtmlTemplate)
    }),
    new webpack.HotModuleReplacementPlugin()
  ],
};
