import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

import options from './options';
import news from './news';
import selectedReddit from './selectedReddit';

const rootReducer = combineReducers({
  options,
  selectedReddit,
  news,
  routing
});

export default rootReducer;
