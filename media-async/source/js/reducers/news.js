import {
  REQUEST_NEWS,
  RECEIVE_NEWS,
  SELECT_NEWS_ARTICLE
} from '../actions'


const initialState = {
  isFetching: false,
  activeArticle: {
    author: '',
    description: '',
    publishedAt: '',
    title: '',
    url: '',
    urlToImage: ''
  },
  data: {
    articles: [],
    sortBy: '',
    source: '',
    status: ''
  }
};

const news = (state = initialState, action) => {

  switch (action.type) {

    case REQUEST_NEWS:
      return { ...state,
        isFetching: true
      };

    case RECEIVE_NEWS:
      return { ...state,
        data: action.news,
        isFetching: false
      };

    case SELECT_NEWS_ARTICLE:
      return { ...state,
        activeArticle: action.article
      };

    default:
      return state
  }
};

export default news;
