import {SELECT_REDDIT, TOGGLE_NEWS_MENU} from '../actions';

const initialState = {
  isShowNewsMenu: false,
};

function options(state = initialState, action) {

  switch (action.type) {

    case SELECT_REDDIT:
      return {
        ...state,
        isShowNewsMenu: false
      };

    case TOGGLE_NEWS_MENU:
      return {
        ...state,
        isShowNewsMenu: action.state
      };

    default:
      return state;
  }
}

export default options;
