import { SELECT_REDDIT } from '../actions';

const initialState = {
  src: '',
  title: '',
  source: ''
};

function selectedReddit(state = initialState, action) {
  switch (action.type) {
    case SELECT_REDDIT:
      return state = action.reddit;
    default:
      return state;
  }
}

export default selectedReddit;
