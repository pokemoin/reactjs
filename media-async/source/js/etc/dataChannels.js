import verge from '../../assets/img/logo/the-verge.png';
import cnn from '../../assets/img/logo/cnn.png';
import bbcSport from '../../assets/img/logo/bbc-sport.png';
import bbcNews from '../../assets/img/logo/bbc-news.png';
import mtvNews from '../../assets/img/logo/mtv-news.png';

const dataChannels = [{
  src: verge,
  title: "The Verge",
  source: 'the-verge'
},{
  src: cnn,
  title: "CNN",
  source: 'cnn'
},{
  src: bbcSport,
  title: "BBC Sport",
  source: 'bbc-sport'
},{
  src: bbcNews,
  title: "BBC News",
  source: 'bbc-news'
},{
  src: mtvNews,
  title: "MTV News",
  source: 'mtv-news'
}];

export default dataChannels;
