import reactLogo from '../../assets/img/logo/react-logo.png';
import reactRouterLogo from '../../assets/img/logo/react-router-logo.png';
import reduxLogo from '../../assets/img/logo/redux-logo.png';
import reduxSagaLogo from '../../assets/img/logo/redux-saga-logo.png';
import es6 from '../../assets/img/logo/es6.png';
import sassLogo from '../../assets/img/logo/sass-logo.png';

const dataInfoLogos = [{
  src: reactLogo,
  title: 'react'
},{
  src: reactRouterLogo,
  title: 'react-router'
}, {
  src: reduxLogo,
  title: 'redux'
}, {
  src: reduxSagaLogo,
  title: 'redux-saga'
}, {
  src: es6,
  title: 'es6'
}, {
  src: sassLogo,
  title: 'sass'
}];

export default dataInfoLogos;
