import React from 'react';
import {Route, Switch} from 'react-router-dom';


import dataURL from './etc/dataURL';

import App from './containers/App';
import NewsPage from './containers/NewsPage';
import NewsItemPage from './containers/NewsItemPage';
import NotFound from './containers/NotFound';

export default <Switch>
  <Route exact path={dataURL.home} component={App} />
  <Route path={`${dataURL.news}/:source/:details/:title`} component={NewsItemPage} />
  <Route path={`${dataURL.news}/:source`} component={NewsPage} />
  <Route component={NotFound}/>
</Switch>;

