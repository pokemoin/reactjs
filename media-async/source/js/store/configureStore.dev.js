import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga'
import rootReducer from '../reducers';
import {createLogger} from 'redux-logger';
import DevTools from '../containers/DevTools';

const configureStore = (initialState) => {

  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(
      rootReducer,
      initialState,
      compose(
          applyMiddleware(
              sagaMiddleware,
              createLogger()
          ),
          DevTools.instrument()
      )
  );

  store.runSaga = sagaMiddleware.run;
  store.close = () => store.dispatch(END);
  return store;
};

export default configureStore;
