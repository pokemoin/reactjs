import React from 'react';
import {Link, Redirect} from 'react-router-dom';
import {bindActionCreators} from 'redux'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as Actions from '../actions';
import dataURL from '../etc/dataURL';

import Info from '../components/blocks/Info';
import Loading from '../components/blocks/Loading';
import Header from '../components/regions/Header';
import NewsMenu from '../components/menus/NewsMenu';
import NewsList from '../components/lists/NewsList';

const NewsPage = (props) => {

  const {selectedReddit, ownProps, options, news, articles, actions} = props;
  const {isFetching} = news;
  const {isShowNewsMenu} = options;
  const cssTheme = `news-menu__sticky index-high${isShowNewsMenu ? ' active' : ''}`;

  return (
      <div className="news-page page">
        { !isFetching && articles.length === 0 ? <Redirect to={dataURL.home}/> : null }
        <Header selected={selectedReddit}
                actions={actions}
        />
        <main className="content">
          {isFetching ?
              <Loading />
              :
              <NewsList articles={articles}
                        ownProps={ownProps}
                        actions={actions}
              />
          }
        </main>
        <Info />
        <NewsMenu actions={actions}
                  cssTheme={cssTheme}
                  selected={selectedReddit}
        />
      </div>
  )
};

NewsPage.propTypes = {
  ownProps: PropTypes.object.isRequired,
  options: PropTypes.object.isRequired,
  news: PropTypes.object.isRequired,
  articles: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  selectedReddit: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {

  const news = state.news;
  const articles = news.data.articles;

  return {
    ownProps: ownProps,
    options: state.options,
    news: news,
    articles: articles,
    selectedReddit: state.selectedReddit
  }
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  actions: bindActionCreators(Actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsPage);
