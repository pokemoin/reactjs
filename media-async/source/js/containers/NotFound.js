import React from 'react';
import {Link} from 'react-router-dom';
import Info from '../components/blocks/Info';

const NotFound = () => {

  return (
      <div className="not-found-page">
        <Info />
        <div className="content">
          <h3 className="title">
            Page not found
          </h3>
          <div className="link">
            <Link to='/'>
              Home
            </Link>
          </div>
        </div>
      </div>
  )
};

export default NotFound;
