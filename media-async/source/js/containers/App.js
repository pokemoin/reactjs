import React from 'react';
import {bindActionCreators} from 'redux'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as Actions from '../actions';

import NewsMenu from '../components/menus/NewsMenu';
import Info from '../components/blocks/Info';

const App = ({selectedReddit, actions}) => {

  const cssTheme = "default";

  return (
      <div className="home-page">
        <Info />
        <NewsMenu actions={actions}
                  cssTheme={cssTheme}
                  selected={selectedReddit}
        />
      </div>
  )
};

App.propTypes = {
  actions: PropTypes.object.isRequired,
  selectedReddit: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
  return {
    selectedReddit: state.selectedReddit
  }
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  actions: bindActionCreators(Actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
