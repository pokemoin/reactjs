import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import routes from '../routes'

const Root = ({store, history}) => (
    <Provider store={store}>
      <Router history={history} children={routes} />
    </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default Root;
