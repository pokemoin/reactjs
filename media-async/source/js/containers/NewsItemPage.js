import React from 'react';
import {Link, Redirect} from 'react-router-dom';
import {bindActionCreators} from 'redux'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as Actions from '../actions';
import dataURL from '../etc/dataURL';

import Info from '../components/blocks/Info';
import NewsItem from '../components/blocks/NewsItem';
import Header from '../components/regions/Header';
import NewsMenu from '../components/menus/NewsMenu';

const NewsItemPage = (props) => {

  const {selectedReddit, article, options, actions} = props;
  const {title} = article;
  const {isShowNewsMenu} = options;
  const cssTheme = `news-menu__sticky index-high${isShowNewsMenu ? ' active' : ''}`;

  return (
      <div className="news-item-page page">
        { title === '' ? <Redirect to={dataURL.home}/> : null }
        <Header selected={selectedReddit}
                actions={actions}
        />
        <main className="content">
          <NewsItem article={article} />
        </main>
        <Info />
        <NewsMenu actions={actions}
                  cssTheme={cssTheme}
                  selected={selectedReddit}
        />
      </div>
  )
};

NewsItemPage.propTypes = {
  article: PropTypes.object.isRequired,
  options: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  selectedReddit: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
  return {
    options: state.options,
    article: state.news.activeArticle,
    selectedReddit: state.selectedReddit
  }
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  actions: bindActionCreators(Actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsItemPage);
