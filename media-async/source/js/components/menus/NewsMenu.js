import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import dataChannels from '../../etc/dataChannels'
import dataURL from '../../etc/dataURL'

const NewsMenuItem = ({selected, channel, actions}) => {

  const {src, title, source} = channel;
  const urlNews = dataURL.news;
  const classActive = selected.title === title ? ' active' : '';

  return (
      <li className={`menu-item${classActive}`}>
        <Link to={`${urlNews}/${source}`}
              onClick={() => actions.selectReddit(channel)}
        >
          <img src={src}
               alt={title}
               title={title}
               className="menu-item__icon"
          />
          <span className="menu-item__title">
            {title}
          </span>
        </Link>
      </li>
  )
};

const NewsMenu = ({selected, actions, cssTheme}) => {
  return (
      <div className={`news-menu ${cssTheme}`}>
        <div className="container">
          <h3 className="menu-title">Please choose a channel</h3>
          <nav className="menu">
            { dataChannels.map((channel, index) =>
                <NewsMenuItem key={`${channel.name}-${index}`}
                              channel={channel}
                              actions={actions}
                              selected={selected}
                />
            )}
          </nav>
          <button className="close"
                  onClick={() => actions.toggleNewsMenu(false)}
          ></button>
        </div>
      </div>
  )
};

NewsMenu.propTypes = {
  selected: PropTypes.object.isRequired,
  cssTheme: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired
};

export default NewsMenu;
