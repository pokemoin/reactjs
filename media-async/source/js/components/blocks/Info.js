import React from 'react';
import dataInfoLogos from '../../etc/dataInfoLogos';

const Info = (props) => {
  return (
      <div className="info">
        <div className="info-button">
          <button>Info</button>
        </div>
        <div className="info-content">
          { dataInfoLogos.map((logo, index) => {

                let src = logo.src;
                let title = logo.title;

                return (
                    <div key={`${title}-${index}`} className="info-content__item">
                      <div className="info-content__item__assets-img">
                        <img src={src} alt={title} title={title}/>
                      </div>
                      <div className="info-content__item__title">
                        {title}
                      </div>
                    </div>
                );
              }
          )}
        </div>
      </div>
  )
};

export default Info;
