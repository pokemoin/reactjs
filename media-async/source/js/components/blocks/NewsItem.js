import React from 'react';
import {Link, Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';

const NewsItem = ({article}) => {

  const {author, description, publishedAt, title, url, urlToImage} = article;

  return (
      <article className="news-article">
        <div className="container">
          <div className="news-article__assets-img">
            <img src={urlToImage} alt={title} title={title}/>
          </div>
          <div className="meta">
            <div className="news-article__title">
              {title}
            </div>
            <div className="news-article__published">
              {publishedAt}
            </div>
            <div className="news-article__author">
              {author}
            </div>
            <div className="news-article__description">
              {description}
            </div>
            <div className="news-article__read-more">
              <a href={url} target="_blank">
                read more
              </a>
            </div>
          </div>
        </div>
      </article>
  )
};

NewsItem.propTypes = {
  article: PropTypes.object.isRequired,
};

export default NewsItem;
