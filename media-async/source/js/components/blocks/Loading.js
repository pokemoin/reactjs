import React from 'react';

const Loading = (props) => {
  return (
      <div className="loading">
        <div className="title">Loading...</div>
        <div className="loader"></div>
      </div>
  )
};

export default Loading;
