import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';

import NewsListItem from '../listItem/NewsListItem';

const NewsList = ({ownProps, articles, actions}) => {
  return (
      <section className="news-list">
        { articles.map((article) =>
            <NewsListItem key={uuid.v1()}
                          article={article}
                          ownProps={ownProps}
                          actions={actions}
            />
        )}
      </section>
  )
};

NewsList.propTypes = {
  ownProps: PropTypes.object.isRequired,
  articles: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
};

export default NewsList;
