import React from 'react';
import PropTypes from 'prop-types';

const Header = ({selected, actions}) => {

  const {src, title} = selected;

  return (
      <header className="header index-middle">
        <div className="container">
          <div className="channel">
            <img src={src}
                 alt={title}
                 title={title}
                 className="channel__icon"
            />
            <span className="channel__title">
              {title}
            </span>
          </div>
          <div className="channel-select">
            <button onClick={() => actions.toggleNewsMenu(true)}>
              Channels
            </button>
          </div>
        </div>
      </header>
  )
};

Header.propTypes = {
  selected: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

export default Header;
