import React from 'react';
import {Link, Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';

const NewsListItem = ({ownProps, article, actions}) => {

  const {author, description, publishedAt, title, url, urlToImage} = article;
  const {match} = ownProps;
  const itemUrl = `${match.url}/details/${title.replace(/\s/g, '-')}`;

  return (
      <article className="news-article">
        <div className="container">
          <Link to={itemUrl}
                onClick={() => actions.selectNewsArticle(article)}
          >
            <div className="news-article__assets-img">
              <img src={urlToImage} alt={title} title={title}/>
            </div>
            <div className="meta">
              <div className="news-article__title">
                {title}
              </div>
            </div>
          </Link>
        </div>
      </article>
  )
};

NewsListItem.propTypes = {
  article: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

export default NewsListItem;
