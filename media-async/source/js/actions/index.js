export const REQUEST_NEWS = 'REQUEST';
export const RECEIVE_NEWS = 'RECEIVE_NEWS';
export const SELECT_REDDIT = 'SELECT_REDDIT';
export const TOGGLE_NEWS_MENU = 'TOGGLE_NEWS_MENU';
export const SELECT_NEWS_ARTICLE = 'SELECT_NEWS_ARTICLE';

export function selectReddit(reddit) {
  return {
    type: SELECT_REDDIT,
    reddit
  }
}

export function requestNews(reddit) {
  return {
    type: REQUEST_NEWS,
    reddit
  }
}

export function receiveNews(reddit, news) {
  return {
    type: RECEIVE_NEWS,
    reddit,
    news
  }
}

export function toggleNewsMenu(state) {
  return {
    type: TOGGLE_NEWS_MENU,
    state
  }
}

export function selectNewsArticle(article) {
  return {
    type: SELECT_NEWS_ARTICLE,
    article
  }
}


