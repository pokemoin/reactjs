import { takeEvery, take, put, call, fork, select, all } from 'redux-saga/effects';
import 'isomorphic-fetch'
import * as actions from '../actions';
import { selectedRedditSelector, newsBySelector } from '../reducers/selectors';

const API_ROOT = 'https://newsapi.org/v1/articles?source=';
const API_KEY = '7922b285c92a4479b55c1bbcddfe053a ';

export function fetchNewsApi(reddit) {
  return fetch(`${API_ROOT}${reddit.source}&sortBy=top&apiKey=${API_KEY}`)
      .then(response => response.json())
}

export function* fetchNews(reddit) {
  yield put( actions.requestNews(reddit));
  const news = yield call(fetchNewsApi, reddit);
  yield put( actions.receiveNews(reddit, news));
}

export function* startup() {
  const selectedReddit = yield select(selectedRedditSelector);
  yield fork(fetchNews, selectedReddit)
}

export default function* root() {
  yield takeEvery('SELECT_REDDIT', startup)
}
