import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import createHistory from 'history/createBrowserHistory';
import { syncHistoryWithStore } from 'react-router-redux';
import configureStore from './store/configureStore';
import Root from './containers/Root';
import rootSaga from './sagas';

const store = configureStore();
store.runSaga(rootSaga);

const history = syncHistoryWithStore(createHistory(), store);

const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Component store={store} history={history}/>
    </AppContainer>,
    document.getElementById('root')
  )
};

render(Root);

if (module.hot) {
  module.hot.accept('./containers/Root', () => { render(Root) })
}
