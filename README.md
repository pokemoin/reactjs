# basic project configuration #

### Used: ###

* "react": "^15.4.2",
* "react-dom": "^15.4.2",
* "react-redux": "^5.0.4",
* "react-router-dom": "^4.0.0",
* "react-router-redux": "^4.0.8",
* "redux": "^3.6.0"

### DEV ###
* "react-hot-loader": "^3.0.0-beta.6",

### Additional ###
* Sass
